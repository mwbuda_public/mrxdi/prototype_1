import ceylon.collection {
	HashSet,
	LinkedList
}

alias Char => Character; 
alias Bool => Boolean;

shared alias KeyType => String|Key;
shared alias PatternType => String|Key|KeyPattern;

Bool isChar(Char x)(Char? c) => return if (is Char c) then c == x else false;

void checkSchemaRule(
	Bool test,
	String errorTitle,
	String detail
) {
	if (!test) {
		throw InvocationException("``errorTitle``: ``detail``");	
	}
}

object patternSchema {

	shared Char separatorCharacter = '.';
	shared Char startWildcardCharacter = '{';
	shared Char endWildcardCharacter = '{';

	shared Set<Char> basicCharacters = HashSet(
		String('a'..'z') 
		+ String('0'..'9') 
		+ "_"
	);
	shared Set<Char> specialCharacters = {
		separatorCharacter,
		startWildcardCharacter,
		endWildcardCharacter
	};
	
	shared validKeySegmentRegex = regex("^[a-z0-9_]+$");
	shared validWildcardNameRegex = regex("^[a-z0-9_]+$");
	shared validPatternSegmentRegex = regex("^[a-z0-9_]+|[{][a-z0-9_]+[}]$");
		
	shared Integer maxKeySegments = 255;
	
	shared Set<Char> illegalCharactersIn(
		String keyPortion,
		Bool includeWildcard = false,
		Bool includeSeparator = false
	) {
		variable Set<Char> result = HashSet(keyPortion).complement(patternSchema.basicCharacters);
		
		if (includeWildcard) {
			result = HashSet(result.filter(not(or(
				isChar(patternSchema.startWildcardCharacter),
				isChar(patternSchema.endWildcardCharacter),
			))));
		}
		
		if (includeSeparator) {
			result = HashSet(result.filter(not(
				isChar(patternSchema.endWildcardCharacter)
			)));
		}
		
		return result;
	}
	
	shared [Bool,String?] isWildcardSegment(String segment) {
		MatchResult? match = regex("^[{]([a-z0-9_]+)[}]$").find(segment);
		return if (exists match) 
			then [true,match.groups[0]
			else [false,null]
		;
	
	}
		
	shared Pattern compilePattern(PatternType raw) => switch(raw) 
		case (is Pattern) then raw
		else Pattern(raw)
	;
	
	shared Key compileKey(KeyType raw) => switch(raw)
		case (is Key) raw
		else Key(raw)
	;	
	
	shared [Pattern*] identifyElligiblePatterns(
		KeyType key,
		{PatternType*} patterns
	) => sort(patterns.collect(compilePattern).filter(
		(Pattern p) => p.match( compileKey(key) )
	));
}

// TODO: evaluate whole key for validity (eg. not exceeding max key segment length)
class Key(KeyType input) 
satisfies Comparable<Key> & Identifiable {

	shared actual String string => resolveInputToString(input).normalized.lowercased;
	
	shared [String+] segments => this.string.split(
		isChar(patternSchema.separatorCharacter), true, false
	).collect(compileSegment);
	
	shared actual Boolean equals(Object that) => this.string == that.string.normalized.lowercased;
	
	shared actual Comparison compare(Key that) {
		//if of unequal size, larger group pattern matches after shorter
		Comparison cmpSize = this.parts.size <=> that.parts.size;
		return (cmpSize != equal) then cmpSize else this.string <=> that.string;
	}
	
	String resolveInputToRaw(KeyType k) => return switch(k)
		case (is Key) k.string
		case (is String) k
	;
	
	String compileSegment(String part) {
		String cleanPart = part.normalized.lowercased;
		Set<Char> illegalChars = patternSchema.illegalCharactersIn(part);
		
		checkSchemaRule(
			illegalChars.empty,
			"unsupported characters in key segment format",
			"``cleanPart`` <- ``illegalChars``"
		);
		
		return cleanPart;
	}
}

// TODO: verify entire pattern validity (eg. dupe wildcards)
class Pattern(PatternType input)
satisfies Comparable<Pattern> & Identifiable {

	shared actual String string => resolveInputToString(input).normalized.lowercased;
	
	shared [String|Wildcard+] segments => LinkedList(this.string.split(
		isChar(patternSchema.separatorCharacter), true, false
	).collect(compileSegment);
	
	shared Bool isAbstract => this.segments.find(isWildcard) exists;
	shared Integer firstWildcardIndex => this.isAbstract then -1 else this.segments.locate(isWildcard).key;
	shared Integer wildcardCount => this.segments.count(isWildcard);
	
	shared actual Boolean equals(Object that) => this.string == that.string.normalized.lowercased;
	
	shared actual Comparison compare(Pattern that) {
		// patterns with later wildcards should be evaluated before those with earlier wildcards
		Comparison cmpFirstWc = this.firstWildcardIndex <=> that.firstWildcardIndex;
		if (cmpFirstWc != equal) {
			return cmpFirstWc.reversed;
		}
		
		// evaluate shorter patterns first
		Comparison cmpSize = this.parts.size <=> that.parts.size;
		if (cmpSize != equal) {
			return cmpSize;
		}
		
		// evaluate less abstract patterns first
		Comparison cmpWcCount = this.wildcardCount <=> that.wildcardCount;
		if (cmpWcCount != equal) {
			return cmpWcCount;
		}
		
		return this.string <=> that.string;	
	}
	
	shared Pattern append(PatternType? that) => if (exists other)
		then Pattern( patternSchema.separator.string.join({
			this.string, that.string
		}))
		else this
	;
	
	shared Pattern prepend(PatternType? that) => if (exists other)
		then Pattern( patternSchema.separator.string.join({
			that.string, this.string
		}))
		else this
	;
	
	shared [Bool,Map<String,String>] match(KeyType raw) {
		Key key;
		
		try {
			key = patternSchema.compileKey(key);
		} 
		catch (Exception e) {
			return [false, emptyMap];
		}
	
		Map<String,String> wildcards = HashMap();
		for (kpart->p in zipEntries(key.segments, this.segments)) {
			Bool isMatch = false;
			switch (p)
			case (is String) {
				isMatch = kpart == p;
			}
			case (is Wildcard) {
				isMatch = true;
				wildcards.put(p.name, kpart);
			}
			
			if (!isMatch) {
				return [false, emptyMap];
			}
		}
		
		return [true,unmodifiableMap(wildcards)];
	}
	
	Boolean isWildcard(String|Boolean x) => x is Wildcard; 
	
	String resolveInputToString(PatternType p) => return switch(p)
		case (is Pattern) p.string
		case (is String) p
	;
	
	String|Wildcard compileSegment(String raw) {
		String cleanPart = part.normalized.lowercased;
		Set<Char> illegalChars = patternSchema.illegalCharactersIn {
			part;
			includeWildcard = true;
		};
		
		checkSchemaRule(
			illegalChars.empty,
			"unsupported characters in key pattern segment format",
			"``cleanPart`` <- ``illegalChars``"
		);
		
		checkSchemaRule(
			patternSchema.validPatternSegmentRegex.test(cleanPart),
			"unsupported syntax in key pattern segment format",
			cleanPart
		
		);
		
		[Bool,String?] wildcard = patternSchema.isWildcardSegment(cleanPart);
		if (wildcard[0]) {
			assert (wildcard[1] is String);
			return Wildcard(wildcard[1]);
		} 
		else {
			return cleanPart;
		}
	}
}

class Wildcard(name) {
	shared String name;
}


