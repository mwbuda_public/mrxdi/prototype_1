

class InjectValue(shared Injection injection)
satisfies Injector {
	shared actual Object inject(InjectRequest rq) => injection(rq);
}

class FixedValue(Object v)
satisfies Injector {
	shared actual Object inject(InjectRequest rq) => v;
}

class InjectError(shared String detail)
satisfies Injector {
	shared actual Object inject(InjectRequest rq) {
		throw injectionError(this.detail);
	}
}

//TODO: finish off instance-of
//TODO: implement setter post procs
//TODO: does some of this work in JS?

class InstanceOf(Class type)
satisfies Injector {
	shared actual Object inject(InjectRequest rq) {
		if (exists bean = type.apply([]) ) {
			return bean;
		}
		else {
			throw injectionError("could not instantiate instance of class: " + type.qualifiedName);	
		}
	}
}