
import ceylon.collection { 
	Map = MutableMap, HMap = HashMap,
	Set = MutableSet, HSet = HashSet 
}

alias Monad => InjectionMonad;

/**
* A context represents a collection of keyed injectors & sub-contexts
*	to instantiate, configure, and provide software components/objects on demand.
*
* Injectors may be keyed by a pattern rather than a static key.
*
*/
class Context() {

	/**
	* all currently defined keys in the context
	*/
	Set<Key> declared = HSet<ContextKey>();
	Map<Pattern, Injector> definitions = HMap<Pattern, Injector>();
	Map<Key, Pattern> keysToPatterns = HMap<Key, Pattern>();
	Map<Key, Monad> keysToInstances = HMap<Key, Monad>();

	/**
	* resolve an injected object from the provided key.
	* @param key : key to resolve
	* @param reload : if true & instance has already been resolved, 
	*	reload & resolve again
	*/
	shared Object? resolve(KeyType key, Boolean reload = false) {
		//TODO
	}
	
	/**
	* define an injection to resolve/create objects for keys matching 
	*	the provided pattern.
	* @param key : context key pattern this injection will match on
	* @param callback : injection logic object
	*/
	shared Context define(PatternType key, Injector callback) {
		//TODO
		return this;
	}

	/**
	* Declare a binding for the provided key exists.
	* Primary use is for forceContextLoad calls when using injectors keyed to a pattern,
	*	to ensure 1+ actual injectons happen for the key/injector pair.
	*/
	shared Context declare(KeyType key) {
		//TODO
		return this;
	}

	shared Context import(KeyType? root, Context source) {
		//TODO
		return this;
	}

	shared Context export(Context target, KeyType? root = null) {
		target.import(root, this);
		return this;
	}

	shared Context addChild(KeyType key, Context childCxt) {
		//TODO
		return this;
	}


	shared Context setParent(KeyType key, Context parentCxt) {
		parentCxt.addChild(key, this);
		return this;
	}

	shared ImmutableMap<Key, Injector> introspectQuery(PatternType key) {
		//TODO
	}

	shared Injector introspect(Key key) {
		//TODO
	}

	shared Context contextLoad(Boolean reload = false) {
		//TODO
		return this;
	}
}
