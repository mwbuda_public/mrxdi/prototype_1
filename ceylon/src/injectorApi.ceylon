
alias Injection => Object(InjectRequest);
alias PostProcess => Object(Object, InjectRequest);

shared Throwable injectionError(String detail) {
	return InvocationException("injection error: " + detail);
}

class InjectRequest(
	shared Key key, 
	shared Pattern pattern,
	shared Map<String,String> matchData,
	shared Context context
) {

}

interface Injector 
satisfies Identifiable {
	shared formal Object inject(InjectRequest rq);
	shared Injector with(PostProcessor|PostProcess pp) => PostProcessed(this,pp); 
}

interface PostProcessor {
	shared formal Object postProcess(Object bean, InjectRequest rq);
}

class PostProcessed(
	Injector parent,
	{PostProcessor|PostProcess+} postProcessors
) satisfies Injector {
	shared actual Object inject(InjectRequest rq) {
		variable Object bean = parent.inject(rq);
		for (pp in postProcessors) {
			bean = switch (pp)
			case (is PostProcessor) pp.postProcess(bean,rq);
			case (is PostProcess) pp(bean,req)
		}
		return bean;
	}
}

class InjectionMonad(
	shared Injector initInjector, 
	shared Object? initValue = null
) {
	variable Object? value = initValue;
	variable Injector currInjector = initInjector;
	shared Injector injector => currInjector;

	shared InjectionMonad rebind(Injector newInjector) {
		if ( currInjector != newInjector ) {
			clear();
			currInjector = newInjector;
		}
		return this;
	}

	shared Object load(InjectRequest rq) {
		Object? v = this.value;
		if (exists v) {
			return v;
		}
		else {
			return this.reload(rq);
		}
	}

	shared Object reload(InjectRequest rq) {
		this.currValue = this.injector.inject(rq);
		return this.currValue;
	}

	shared InjectionMonad clear() {
		this.value = null;
		return this;
	}
}

